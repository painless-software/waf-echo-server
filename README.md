<!-- # This file is managed by Concierge, do not edit. -->

# Web Application Firewall

WAF deployment and configuration for waf-demo echo-server
Based on [vshn/modsecurity](https://github.com/vshn/modsecurity-docker).

## Initial Setup (GitLab + OpenShift)

> **Info**: Steps 1. - 3. only need to be done once per OpenShift project. If the ServiceAccount `gitlab-ci` already exists and has the correct access rights, proceed with step 4.

1. Create a service account in your OpenShift project:

    ```console
    oc -n waf-demo create sa gitlab-ci
    oc -n waf-demo policy add-role-to-user edit -z gitlab-ci
    ```

1. Configure the [Kubernetes integration](https://gitlab.com/vshn/waf-demo/waf-echo-server/clusters)
    in your GitLab project adding the `token` value from the `gitlab-ci-token`
    secret:

    ```console
    oc -n waf-demo sa get-token gitlab-ci
    ```

    Operations > Kubernetes > Add existing cluster
    * Kubernetes cluster name: `APPUiO Cluster`
    * API URL: `https://console.appuio.ch`
    * Service Token: `$gitlab-ci-token`
    * GitLab-managed cluster: `disable`
    > **Warning**: Make sure "GitLab-managed cluster" is unchecked!

## Customize Rules

ModSecurity custom rules are located in [deployment/base/rules/](deployment/base/rules).
You can add your rules to any file, or create new files, called `*.conf`
in the *before-crs* and *after-crs* folders.

Please consult the Docker base image [source code](
https://github.com/vshn/modsecurity-docker/tree/master/v3.1/custom-rules)
to understand the default configuration.

## Customize ModSecurity Settings

ModSecurity settings can be configured via environment variables. Please consult the Docker base image [documentation](https://github.com/vshn/modsecurity-docker#configuration) to see available configuration.
All environment variables from the file [modsecurity.env](deployment/base/config/modsecurity.env) are passed to the deployed ModSecurity container.

## CI/CD Process

We have 3 environments corresponding to the following projects on OpenShift:
* waf-demo-development
* waf-demo-integration
* waf-demo-production

The following strategy is implemented:
* Any merge request triggers a deployment (of the feature branch) as a review app on `development`. The WAF is deployed with a unique name and a new URL.
* Once a MR is merged into `master` it triggers a deployment on `development` and `integration`.
* To trigger a deployment on `production` push a Git tag, e.g.

  ```console
  git checkout master
  git tag v1.0.0
  git push --tags
  ```

## Metrics

The WAF provides 2 Prometheus metrics endpoints:

* Apache exporter
  * HTTP request durations
  * HTTP request sizes
  * Reachable on endpoint :9117/metrics
* Netdata Web log
  * Response Codes
  * HTTP Method
  * Requests per URL
  * Response Codes Families
  * Unique clients
  * May have a minute of startup time
  * Reachable on endpoint :19999/api/v1/allmetrics?format=prometheus&prefix=waf&source=raw

## Local Testing

A docker-compose setup can be used to run and test ModSecurity locally.

```console
docker-compose up
```

The setup starts the ModSecurity container and an [httpbin](https://httpbin.org/) instance as backend. Requests can be tested using `curl` and/or a broswer:
```console
curl -i localhost:8080/anything/some/test
```
